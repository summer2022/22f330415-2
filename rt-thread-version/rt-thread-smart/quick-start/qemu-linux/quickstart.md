# QEMU 快速上手 (Linux)

本教程在 Ubuntu 平台上使用 QEMU 快速上手 RT-Smart，运行 RT-Smart 用户态应用，内核基于 qemu-vexpress-a9。

## 如何构建用户态应用

### 下载代码

下载 smart 用户态应用代码：

```
git clone https://github.com/RT-Thread/userapps.git
```

源码目录说明：

```
.
├── apps                  ---- app 体验示例
├── configs               ---- 不同平台 app 的通用配置
├── linker_scripts        ---- 编译 app 使用的链接脚本
├── prebuilt              ---- 预编译好的内核镜像
├── rtconfig.h            ---- app 使用的公共配置文件
├── sdk                   ---- 开发 app 使用的 sdk
├── smart-env.bat         ---- 配置 Win 上环境变量的脚本
├── smart-env.sh          ---- 配置 Linux 上环境变量的脚本
└── tools                 ---- 开发 app 使用的脚本工具
     ├── get_toolchain.py ---- 下载工具链的脚本
     └── gnu_gcc          ---- 下载下来的工具链存放的路径
```

### 配置工具链

在 userapps\tools 目录下运行 get_toolchain.py 的脚本，会下载对应的工具链并展开到 userapps\tools\gun_gcc 目录。后面的工具链名称可以是 arm | riscv64。

本文以 ARM 平台为例，输入下面的命令：

```
python get_toolchain.py arm
```

在 userapps 目录下， 运行 smart-env.sh 配置工具链路径，配置完会显示配置好的工具链路径。

```
source smart-env.sh arm
```

![image-20221021144625013](figures/set.png)

### 编译用户态应用

在 userapps 目录下使用 scons 编译，编译顺利的话，将在 root 文件夹中得到一系列可执行 elf 文件。

![img](figures/build_app.png)

![image-20221021145335403](figures/build_app-res.png)

## 运行用户态应用

在本仓库的 prebuilt 目录下存放有预构建好的针对 QEMU ARM 平台的内核镜像 qemu-vexpress-a9\rtthread.bin，可以直接运行体验。

### 将待调试的 app 拷贝到 sd.bin

如果 sd.bin 不存在，可以通过下面方法创建并格式化：

```
dd if=/dev/zero of=sd.bin bs=1024 count=65536
mkfs.vfat sd.bin
```

然后后拷贝 app 到 sd.bin 中 (调试过程中每次修改重新编译 app 后，都需要更新 sd.bin 里面的 app 文件)：

```
sudo mount sd.bin /mnt
sudo cp root/bin/hello.elf /mnt/hello.elf
sudo umount /mnt
```

![image-20221021153444835](figures/sd.png)

### 运行 QEMU

将 sd.bin 复制到 userapps\prebuilt\qemu-vexpress-a9 中，终端也切换到该目录下

![image-20221021152537185](figures/prebuilt-file.png)

在 userapps\prebuilt\qemu-vexpress-a9 目录下，执行 `./qemu-nographic.sh`。

![image-20221024180626302](figures/qemu_run.png)

Smart 运行起来后输入 ls 可以看到我们存储到 QEMU SD 卡里的文件和文件夹了。

![image-20221024180816610](figures/qemu_run2.png)

在最后执行了 hello 示例，输出 "hello world!"。

执行 ctrl a,x 可退出 QEMU。

## 构建内核镜像

当需要更新内核镜像文件时，查看本节内容。

下载 rt-thread 源码（如有则跳过），之后切换到 rt-smart 分支并从远端同步更新。

```
git clone https://github.com/RT-Thread/rt-thread.git

git checkout rt-smart
# update kernel's rt-smart branch to the latest version
git pull origin rt-smart
```

基于 rt-thread 仓库 rt-smart 分支的 qemu-vexprss-a9 BSP 构建内核镜像，在该目录下执行 scons 编译。

![img](figures/build_kernel1.png)

![image-20221021153136199](figures/build_kernel2.png)

将生成的内核镜像 rtthread.bin 更新到 userapps\prebuilt\ qemu-vexpress-a9 目录即可。



