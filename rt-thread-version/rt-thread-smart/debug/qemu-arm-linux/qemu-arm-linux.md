# 使用 VSCode 调试用户态应用 (arm-Linux)

本教程的内容是如何在 Ubuntu 上使用 VSCode 调试 RT-Smart 内核及用户态应用，基于 qemu-vexpress-a9 BSP 完成。

## 准备工作

1. 下载源码：

    - rt-thread(https://github.com/RT-Thread/rt-thread.git) 切换到 rt-smart 分支。

    ```bash
    $ git clone https://github.com/RT-Thread/rt-thread.git
    $
    $ git checkout rt-smart
    $
    # update kernel's rt-smart branch to the latest version
    $ git pull origin rt-smart
    ```

    - 用户态应用程序 (https://github.com/RT-Thread/userapps.git)

    ```bash
    $ git clone https://github.com/RT-Thread/userapps.git
    ```

2. 安装 VSCode：安装 VSCode 并安装 c/c++ 扩展插件。

    ```bash
    $ sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
    $ sudo apt-get update
    $ sudo apt-get install ubuntu-make
    $ umake ide visual-studio-code

    # 提示输入 a 即可
    ```

3. 安装 GDB：gcc-arm-none-eabi 已经不再包含 arm-none-eabi-gdb 调试工具，需要去官方网站下载对应的 toolchain 压缩包，然后解压到 /opt 下：

    ```bash
    $ wget https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2
    $
    $ sudo tar xf gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 -C /opt
    ```

    然后设置 GDB 环境变量设置，打开 ~/.bashrc

    ```bash
    $ sudo gedit ~/.bashrc
    ```
    在里面设置 PATH 环境变量 `export PATH=$PATH:/opt/gcc-arm-none-eabi-10-2020-q4-major/bin:/opt/gcc-arm-none-eabi-10-2020-q4-major/arm-none-eabi/bin`，保存。

    ![image-20221026140628406](figures/gdb-path.png)

## 编译 userapps 与内核

在 userapps 中，配置 gcc 工具链并编译 app：
1. 在 `userapps\tools` 目录下，使用如下 python 命令 `python get_toolchain.py arm` 下载工具链（若之前执行过则无需再次执行）；
2. 在 `userapps` 目录下，使用命令 `source smart-env.sh arm` 设置工具链路径；
3. 在 `userapps` 目录下，使用命令 `scons` 编译。编译顺利的话，将在 `userapps\root` 文件夹中得到一系列可执行 elf 文件。

在 `\rt-thread\bsp\qemu-vexpress-a9` 目录下，编译系统内核：
1. 使用命令 `scons` 编译，会生成 rtthread.elf 与 rtthread.bin；
2. 使用命令 `./qemu-nographic.sh`，测试 qemu 正常运行后，使用 ctrl a,x 结束运行。

## VSCode 配置

要想使用 VSCode 调试用户态应用，需要先在工程路径下添加调试配置。

1. 首先在终端使用命令 `code . `，使用 VSCode 打开该目录。
2. 修改 `\rt-thread\bsp\qemu-vexpress-a9` 目录下 `.vscode` 文件夹中的 `launch.json` 配置，如下所示（需要更新实际的 gdb 路径）：

```json
{
    "version": "0.2.0",
    "configurations": [
      {
        "name": "Debug smart @ Linux",
        "type": "cppdbg",
        "request": "launch",
        "args": [],
        "stopAtEntry": true,
        "externalConsole": true,
        "cwd": "${workspaceRoot}",
        "program": "${workspaceRoot}/rtthread.elf",
        "serverLaunchTimeout": 2000,
        "miDebuggerPath":"/opt/gcc-arm-none-eabi-10-2020-q4-major/bin/arm-none-eabi-gdb",
        "miDebuggerServerAddress": ":1234",
        "setupCommands": [
          {
            "text": "cd ${workspaceRoot}"
          },
          {
            "text": "file rtthread.elf"
          },
          {
            "text": "break main"
          }
        ],
        "customLaunchSetupCommands": [],
        "launchCompleteCommand": "exec-run"
      },
    ]
  }
```

### 将待调试的 app 拷贝到 sd.bin

如果 sd.bin 不存在，可以通过下面方法创建并格式化：

```bash
$ dd if=/dev/zero of=sd.bin bs=1024 count=65536
$ mkfs.vfat sd.bin
```

然后后拷贝 app 到 sd.bin 中 (调试过程中每次修改重新编译 app 后，都需要更新 sd.bin 里面的 app 文件)：

```bash
$ sudo mount sd.bin /mnt
$ sudo cp ../../../userapps/root/bin/hello.elf /mnt/hello.elf
$ sudo umount /mnt
```

## 调试用户态应用

这部分开始正式调试用户态应用，先进入内核的调试界面，然后添加用户态应用的符号表，再在内核中运行用户态应用即可进入用户态应用调试，具体步骤如下：

1. 在 VSCode 终端输入 `./qemu-dbg.sh`，此时弹出 qemu 窗口。可以看到启动过程被挂起，等待调试前端来连接。

2. 在 VSCode 中按下 F5 开始调试内核，运行的代码将停在 main 断点处，并且控制台打印 rt-thread logo，表示系统已经正常启动，文件系统也已经成功挂载。

    ![image-20221026104404817](figures/vsc-debug.png)

3. 断点在 main 处时，可以加载待调试应用程序的符号，如调试 hello.elf，在 VSCode 调试控制台输入如下命令后回车（注意调整 elf 文件的正确的相对路径） `-exec add-symbol-file ../../../userapps/root/bin/hello.elf`，之后输入 `-exec b main` 命令并回车，这将会在 app main 上打断点。

    ![image-20221026103535098](figures/debug.png)

4. 按下 F5 全速运行程序，这时系统正常运行，可以在终端输入 MSH 调试命令。在终端输入用户态应用的文件路径执行相应的用户态应用，如 `mnt/hello.elf`。应用会被加载运行，并自动停在应用程序入口处。从 VSCode 可以看到应用的源码文件被打开，并且断点停在应用入口 main 处。

    ![image-20221026104738963](figures/brk-hello-main.png)

5. 后续就可以单步调试用户态应用了。单步运行后，应用代码执行的打印将显示在终端上。

    ![image-20221026104855575](figures/debug-app-1.png)

