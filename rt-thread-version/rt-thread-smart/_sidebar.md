<!-- docs/_sidebar.md -->

- **RT-Thread Smart版本**

- 简介
  - [smart简介](/rt-thread-version/rt-thread-smart/introduction/rt-smart-intro/rt-smart-intro.md)
  - [smart进程概述](/rt-thread-version/rt-thread-smart/introduction/rt-smart-lwp/rt-smart-lwp.md)

- 快速上手
  - [QEMU快速上手(Win)](/rt-thread-version/rt-thread-smart/quick-start/qemu-win/quickstart.md)
  - [QEMU快速上手(Linux)](/rt-thread-version/rt-thread-smart/quick-start/qemu-linux/quickstart.md)
  
- 开发调试
  - [使用VSCode调试用户态应用(Win)](/rt-thread-version/rt-thread-smart/debug/qemu-arm-win/qemu-arm-win.md)
  - [使用VSCode调试用户态应用(Linux)](/rt-thread-version/rt-thread-smart/debug/qemu-arm-linux/qemu-arm-linux.md)
  - [RT-Thread smart插件](/rt-thread-version/rt-thread-smart/debug/vsc-plug-in/vsc-plug-in.md)

<!-- - 系统服务 -->
  <!-- - [web server]() -->

- 应用开发
  - [移植Linux应用到smart](/rt-thread-version/rt-thread-smart/application-note/port-app/port-app.md)
  - [使用VSCode开发GUI应用](/rt-thread-version/rt-thread-smart/application-note/sdl2/sdl2.md)
  - [基于FFmpeg+SDL2实现视频播放](/rt-thread-version/rt-thread-smart/application-note/sdl2_ffmpeg/sdl2_ffmpeg.md)
  - [应用合集](/rt-thread-version/rt-thread-smart/application-note/app-list/app-list.md)


